# Image Machine
Image Machine is the app of machine documentation which contains some machine image on it. The user can add machine data as much as possible and then it will be shown on the main page as a list. This app is fully on the offline mode. So it works properly without a network connection.

## Point of features
1. Machine data management. The user can add, edit, and remove it as needed.
2. Adding the machine picture which has two ways both of pick from a gallery or take a new picture through the camera.
3. According to point number 2, so the app needs some permissions such as read external file, write an external file, and access the camera.
4. Scan QRCode for showing the machine detail.

## Developer Notes
This app is still on the basic development. It could need some performance improvements, saving the resources, and more UX upgrades.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)