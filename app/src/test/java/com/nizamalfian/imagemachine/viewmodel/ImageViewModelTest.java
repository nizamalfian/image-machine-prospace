package com.nizamalfian.imagemachine.viewmodel;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import com.nizamalfian.imagemachine.model.MachineRepository;
import com.nizamalfian.imagemachine.model.entity.MachineImage;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static com.nizamalfian.imagemachine.model.FakeMachineData.getImagesByte;
import static org.mockito.Mockito.*;

/**
 * Created by nizamalfian on 24/08/2019.
 */
@RunWith(MockitoJUnitRunner.class)
public class ImageViewModelTest {
    @Rule
    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();
    private ImageViewModel viewModel=mock(ImageViewModel.class);
    private List<byte[]>dummyImages=getImagesByte();
    private byte[]imageAdded=dummyImages.get(0);
    private byte[]imageRemoved=dummyImages.get(1);

    @Before
    public void setUp() {
        viewModel.addImage(imageAdded);
        viewModel.addImage(imageRemoved);
    }

    @Test
    public void testGetImages() {
        List<byte[]>resultImages=new ArrayList<>();
        resultImages.add(imageAdded);
        resultImages.add(imageRemoved);
        MutableLiveData<List<byte[]>>imagesObserve=new MutableLiveData<>();
        imagesObserve.setValue(resultImages);
        when(viewModel.getImages()).thenReturn(imagesObserve);
        Observer<List<byte[]>> observer = mock(Observer.class);
        viewModel.getImages().observeForever(observer);
        verify(observer).onChanged(resultImages);
    }
}