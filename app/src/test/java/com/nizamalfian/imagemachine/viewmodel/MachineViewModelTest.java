package com.nizamalfian.imagemachine.viewmodel;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import com.nizamalfian.imagemachine.model.FakeMachineData;
import com.nizamalfian.imagemachine.model.MachineRepository;
import com.nizamalfian.imagemachine.model.entity.Machine;
import com.nizamalfian.imagemachine.model.entity.MachineImage;
import com.nizamalfian.imagemachine.model.entity.MachineWithImages;
import com.nizamalfian.imagemachine.model.vo.Resource;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.List;

import static org.mockito.Mockito.*;

/**
 * Created by nizamalfian on 19/08/2019.
 */
public class MachineViewModelTest {
    @Rule
    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();
    private MachineViewModel viewModel;
    private MachineRepository machineRepository = mock(MachineRepository.class);
    private List<Machine>dummyMachines= FakeMachineData.getMachineDummy();
    private List<MachineImage>dummyImages= FakeMachineData.getImagesDummy();
    private long dummyMachineId=dummyMachines.get(0).getMachineId();
    private MachineWithImages dummyMachineWithImages=FakeMachineData.getMachineWithImages();

    @Before
    public void setUp() throws Exception {
        viewModel=new MachineViewModel(machineRepository);
        viewModel.setMachineId(dummyMachineId);
    }

    @Test
    public void testGetMachines() {
        Resource<List<Machine>>resource=Resource.success(dummyMachines);
        MutableLiveData<Resource<List<Machine>>> machines = new MutableLiveData<>();
        machines.setValue(resource);
        when(machineRepository.getMachines()).thenReturn(machines);
        Observer<Resource<List<Machine>>> observer = mock(Observer.class);
        viewModel.getMachines().observeForever(observer);
        verify(observer).onChanged(resource);
    }

    @Test
    public void testGetImages() {
        Resource<List<MachineImage>>resource=Resource.success(dummyImages);
        MutableLiveData<Resource<List<MachineImage>>>images=new MutableLiveData<>();
        images.setValue(resource);
        when(machineRepository.getImages(dummyMachineId)).thenReturn(images);
        Observer<Resource<List<MachineImage>>> observer = mock(Observer.class);
        viewModel.images.observeForever(observer);
        verify(observer).onChanged(resource);
    }

    @Test
    public void testMachineDetail() {
        Resource<MachineWithImages>resource=Resource.success(dummyMachineWithImages);
        MutableLiveData<Resource<MachineWithImages>>machineDetail=new MutableLiveData<>();
        machineDetail.setValue(resource);
        when(machineRepository.getMachineWithImages(dummyMachineId)).thenReturn(machineDetail);
        Observer<Resource<MachineWithImages>>observer=mock(Observer.class);
        viewModel.machineDetailById.observeForever(observer);
        verify(observer).onChanged(resource);
    }
}
