package com.nizamalfian.imagemachine.model;

import com.nizamalfian.imagemachine.model.entity.Machine;
import com.nizamalfian.imagemachine.model.entity.MachineImage;
import com.nizamalfian.imagemachine.model.entity.MachineWithImages;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by nizamalfian on 19/08/2019.
 */
public class FakeMachineData {
    private static Machine machine1=new Machine("NNN", "PPP", "126", Calendar.getInstance().getTime());
    private static Machine machine2=new Machine("III", "UUU", "534",Calendar.getInstance().getTime());
    private static Machine machine3=new Machine("ZZZ", "TTT", "279",Calendar.getInstance().getTime());
    private static Machine machine4=new Machine("AAA", "RRR", "126", Calendar.getInstance().getTime());
    private static Machine machine5=new Machine("MMM", "III", "534",Calendar.getInstance().getTime());
    private static Machine machine6=new Machine("III", "WWW", "279",Calendar.getInstance().getTime());
    private static Machine machine7=new Machine("RRR", "LLL", "126", Calendar.getInstance().getTime());
    private static Machine machine8=new Machine("UUU", "AAA", "534",Calendar.getInstance().getTime());
    private static Machine machine9=new Machine("LLL", "NNN", "279",Calendar.getInstance().getTime());
    private static Machine machine10=new Machine("FFF", "CCC", "126", Calendar.getInstance().getTime());
    private static Machine machine11=new Machine("XXX", "YYY", "534",Calendar.getInstance().getTime());
    private static Machine machine12=new Machine("SSS", "CCC", "279",Calendar.getInstance().getTime());

    public static MachineWithImages getMachineWithImages(){
        return new MachineWithImages(getMachineDummy().get(0),getImagesDummy());
    }

    public static List<Machine>getMachineDummy(){
        ArrayList<Machine>machines=new ArrayList<>();
        machines.add(machine1);
        machines.add(machine2);
        machines.add(machine3);
        machines.add(machine4);
        machines.add(machine5);
        machines.add(machine6);
        machines.add(machine7);
        machines.add(machine8);
        machines.add(machine9);
        machines.add(machine10);
        machines.add(machine11);
        machines.add(machine12);
        return machines;
    }

    public static List<byte[]>getImagesByte(){
        ArrayList<byte[]>images=new ArrayList<>();
        byte[]imagesByte=new byte[1];
        images.add(imagesByte);
        images.add(imagesByte);
        images.add(imagesByte);
        return images;
    }

    public static List<MachineImage>getImagesDummy(){
        ArrayList<MachineImage>images=new ArrayList<>();
        byte[]bytes=new byte[1];
        bytes[0]=Byte.parseByte("123");
        images.add(new MachineImage(
                machine1.getMachineId(),
                bytes
        ));
        images.add(new MachineImage(
                machine1.getMachineId(),
                bytes
        ));
        images.add(new MachineImage(
                machine2.getMachineId(),
                bytes
        ));
        images.add(new MachineImage(
                machine2.getMachineId(),
                bytes
        ));
        images.add(new MachineImage(
                machine3.getMachineId(),
                bytes
        ));
        images.add(new MachineImage(
                machine3.getMachineId(),
                bytes
        ));
        return images;
    }
}
