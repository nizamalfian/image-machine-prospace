package com.nizamalfian.imagemachine.utils.view;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;

import java.util.List;

/**
 * Created by nizamalfian on 24/08/2019.
 */
public class ButtonUtil {

    private static boolean areFormsFilled(ButtonEnablingCallback callback) {
        return callback.isButtonEnabled();
    }

    public static void buttonEnabling(List<EditText>editTexts, Button button, ButtonEnablingCallback callback) {
        for(EditText editText:editTexts){
            buttonEnabling(editText,button,callback);
        }
    }

    public static void buttonEnabling(Button button,ButtonEnablingCallback callback){
        button.setEnabled(areFormsFilled(callback));
    }

    private static void buttonEnabling(EditText editText, Button button, ButtonEnablingCallback callback) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                button.setEnabled(areFormsFilled(callback));
            }
        });
    }

    public interface ButtonEnablingCallback{
        boolean isButtonEnabled();
    }
}
