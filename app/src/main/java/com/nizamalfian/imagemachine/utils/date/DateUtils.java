package com.nizamalfian.imagemachine.utils.date;

import android.annotation.SuppressLint;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by nizamalfian on 20/08/2019.
 */
public class DateUtils {
    private static final String DATE_FORMAT="dd-MM-yyyy hh:mm";

    @SuppressLint("SimpleDateFormat")
    public static Date convertToDate(String date){
        try {
            return new SimpleDateFormat(DATE_FORMAT).parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            throw new IllegalArgumentException("Invalid date type");
        }
    }

    public static String convertToString(Calendar calendar){
        return new SimpleDateFormat(DATE_FORMAT,Locale.getDefault()).format(calendar.getTime());
    }

    public static String convertToString(Date date){
        return new SimpleDateFormat(DATE_FORMAT, Locale.getDefault()).format(date);
    }

    public static String getDate(Date date){
        return new SimpleDateFormat(DATE_FORMAT.split(" ")[0], Locale.getDefault()).format(date);
    }

    public static String getHour(Date date){
        return new SimpleDateFormat(DATE_FORMAT.split(" ")[1], Locale.getDefault()).format(date);
    }
}
