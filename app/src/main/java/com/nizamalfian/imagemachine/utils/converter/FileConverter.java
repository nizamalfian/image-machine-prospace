package com.nizamalfian.imagemachine.utils.converter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Environment;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by nizamalfian on 24/08/2019.
 */
public class FileConverter {
    public static File createImageFile(Activity activity) throws IOException {
        @SuppressLint("SimpleDateFormat") String date=new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        return File.createTempFile("JPEG_"+date,".jpg",activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES));
    }
}
