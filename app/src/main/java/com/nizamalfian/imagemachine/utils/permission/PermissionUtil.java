package com.nizamalfian.imagemachine.utils.permission;

import android.os.Build;
import android.widget.Toast;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.nizamalfian.imagemachine.R;
import com.nizamalfian.imagemachine.base.BaseActivity;

import java.util.List;

/**
 * Created by nizamalfian on 27/08/2019.
 */
public class PermissionUtil {
    public interface Callback{
        void onPermissionAllowed();
    }

    public static void permission(BaseActivity activity,PermissionUtil.Callback callback,String...permissions){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            Dexter.withActivity(activity).withPermissions(permissions).withListener(new MultiplePermissionsListener() {
                @Override
                public void onPermissionsChecked(MultiplePermissionsReport report) {
                    if (report.areAllPermissionsGranted()) {
                        callback.onPermissionAllowed();
                    }
                }

                @Override
                public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                    token.continuePermissionRequest();
                }
            }).withErrorListener(error -> Toast.makeText(activity, activity.getString(R.string.something_error), Toast.LENGTH_SHORT).show()).onSameThread().check();
        } else
            callback.onPermissionAllowed();
    }
}
