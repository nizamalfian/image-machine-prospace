package com.nizamalfian.imagemachine.utils.widget;

import android.content.Context;
import android.graphics.Rect;
import android.util.TypedValue;
import android.view.View;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by nizamalfian on 20/08/2019.
 */
public class SpaceItemDecoration extends RecyclerView.ItemDecoration {
    private int space;
    private Context context;

    public SpaceItemDecoration(int space, Context context) {
        this.space = space;
        this.context = context;
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        int position = parent.getChildAdapterPosition(view);
        boolean isLast = position==state.getItemCount()-1;
        if(isLast){
            outRect.bottom = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (float)space,context.getResources().getDisplayMetrics());
            outRect.top=0;
        }
    }
}
