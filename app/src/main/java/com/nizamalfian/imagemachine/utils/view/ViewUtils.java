package com.nizamalfian.imagemachine.utils.view;

import androidx.recyclerview.widget.RecyclerView;
import com.nizamalfian.imagemachine.utils.widget.MyDividerItemDecoration;
import com.nizamalfian.imagemachine.utils.widget.SpaceItemDecoration;

/**
 * Created by nizamalfian on 20/08/2019.
 */
public class ViewUtils {
    public static void setDividerItem(RecyclerView recyclerView){
        recyclerView.addItemDecoration(new MyDividerItemDecoration(recyclerView.getContext()));
    }

    public static void setBottomPadding(RecyclerView recyclerView,int padding){
        recyclerView.addItemDecoration(new SpaceItemDecoration(padding,recyclerView.getContext()));
    }
}
