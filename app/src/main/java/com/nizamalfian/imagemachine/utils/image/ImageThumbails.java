package com.nizamalfian.imagemachine.utils.image;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

import java.util.ArrayList;

/**
 * Created by nizamalfian on 26/08/2019.
 */
public class ImageThumbails {
    public static ArrayList<String> getGalleryImagePaths(Context context) {
        Uri uri;
        Cursor cursor;
        int columnIndexData;
        ArrayList<String> listOfAllImages = new ArrayList<>();
        String absolutePathOfImage;
        uri = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI;

        String[] projection = {MediaStore.MediaColumns.DATA,
                MediaStore.Images.Media.BUCKET_DISPLAY_NAME};

        cursor = context.getContentResolver().query(uri, projection, null, null, null);

        columnIndexData = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        while (cursor.moveToNext()) {
            absolutePathOfImage = cursor.getString(columnIndexData);

            listOfAllImages.add(absolutePathOfImage);
        }
        return listOfAllImages;
    }
}
