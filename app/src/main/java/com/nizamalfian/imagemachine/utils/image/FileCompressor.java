package com.nizamalfian.imagemachine.utils.image;

import android.content.Context;
import android.graphics.Bitmap;
import kotlin.jvm.Throws;

import java.io.File;
import java.io.IOException;

/**
 * Created by nizamalfian on 25/08/2019.
 */
public class FileCompressor {
    private int maxWidth=1600;
    private int maxHeight=1600;
    private Bitmap.CompressFormat compressFormat=Bitmap.CompressFormat.JPEG;
    private int quality=80;
    private String destinationDirectoryPath;

    public FileCompressor(Context context){
        destinationDirectoryPath=context.getCacheDir().getPath()+ File.separator+"images";
    }

    public File compressToFile(File file) throws IOException {
        return ImageUtils.compressImage(file,maxWidth,maxHeight,compressFormat,quality,destinationDirectoryPath+File.separator+file.getName());
    }
}
