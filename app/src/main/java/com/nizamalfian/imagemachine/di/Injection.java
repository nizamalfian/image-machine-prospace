package com.nizamalfian.imagemachine.di;

import android.app.Application;
import com.nizamalfian.imagemachine.model.MachineRepositoryService;
import com.nizamalfian.imagemachine.model.MachineRepository;
import com.nizamalfian.imagemachine.model.database.MachineDatabase;
import com.nizamalfian.imagemachine.utils.executor.AppExecutors;

/**
 * Created by nizamalfian on 19/08/2019.
 */
public class Injection {
    public static MachineRepositoryService provideRepository(Application application) {
        return MachineRepository.getInstance(application,
                MachineDatabase
                        .getInstance(application.getApplicationContext())
                        .machineDao(),
                new AppExecutors()
        );
    }
}
