package com.nizamalfian.imagemachine.model.vo;

public enum Status {
    SUCCESS,
    ERROR,
    LOADING
}
