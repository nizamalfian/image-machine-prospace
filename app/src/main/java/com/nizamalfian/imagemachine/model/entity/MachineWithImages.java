package com.nizamalfian.imagemachine.model.entity;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;

/**
 * Created by nizamalfian on 19/08/2019.
 */
public class MachineWithImages {
    @Embedded
    private Machine machine;
    @Relation(parentColumn = "machineId",entityColumn = "machineId")
    private List<MachineImage>images;

    public MachineWithImages(Machine machine, List<MachineImage> images) {
        this.machine = machine;
        this.images = images;
    }

    public Machine getMachine() {
        return machine;
    }

    public List<MachineImage> getImages() {
        return images;
    }
}
