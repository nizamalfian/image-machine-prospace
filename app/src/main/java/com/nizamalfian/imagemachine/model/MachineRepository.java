package com.nizamalfian.imagemachine.model;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.nizamalfian.imagemachine.model.database.MachineDao;
import com.nizamalfian.imagemachine.model.entity.Machine;
import com.nizamalfian.imagemachine.model.entity.MachineImage;
import com.nizamalfian.imagemachine.model.entity.MachineWithImages;
import com.nizamalfian.imagemachine.model.pojo.Gallery;
import com.nizamalfian.imagemachine.model.vo.NetworkBoundResource;
import com.nizamalfian.imagemachine.model.vo.Resource;
import com.nizamalfian.imagemachine.utils.executor.AppExecutors;
import com.nizamalfian.imagemachine.utils.image.ImageThumbails;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nizamalfian on 19/08/2019.
 */
public class MachineRepository implements MachineRepositoryService {
    private static MachineRepository INSTANCE;

    public static MachineRepository getInstance(Application application,MachineDao machineDao,AppExecutors appExecutors) {
        if (INSTANCE == null) {
            INSTANCE = new MachineRepository(application, machineDao, appExecutors);
        }
        return INSTANCE;
    }

    private Application application;
    private MachineDao machineDao;
    private AppExecutors appExecutors;

    public MachineRepository(Application application, MachineDao machineDao, AppExecutors appExecutors) {
        this.application = application;
        this.machineDao = machineDao;
        this.appExecutors = appExecutors;
    }

    @Override
    public void saveMachineWithImages(Machine machine, List<byte[]> images) {
        Runnable runnable = () -> machineDao.saveMachineAndImages(machine,images);
        appExecutors.diskIO().execute(runnable);
    }

    @Override
    public void remove(Machine machine) {
        Runnable runnable = () -> machineDao.remove(machine);
        appExecutors.diskIO().execute(runnable);
    }

    @Override
    public LiveData<Resource<List<Machine>>> getMachines() {
        return new NetworkBoundResource<List<Machine>>() {
            @Override
            protected LiveData<List<Machine>> loadFromDB() {
                return machineDao.getMachines();
            }
        }.asLiveData();
    }

    @Override
    public LiveData<Resource<List<Machine>>> getMachinesAscendingName() {
        return new NetworkBoundResource<List<Machine>>() {
            @Override
            protected LiveData<List<Machine>> loadFromDB() {
                return machineDao.getMachinesAscendingName();
            }
        }.asLiveData();
    }

    @Override
    public LiveData<Resource<List<Machine>>> getMachinesDescendingName() {
        return new NetworkBoundResource<List<Machine>>() {
            @Override
            protected LiveData<List<Machine>> loadFromDB() {
                return machineDao.getMachinesDescendingName();
            }
        }.asLiveData();
    }

    @Override
    public LiveData<Resource<List<Machine>>> getMachinesAscendingType() {
        return new NetworkBoundResource<List<Machine>>() {
            @Override
            protected LiveData<List<Machine>> loadFromDB() {
                return machineDao.getMachinesAscendingType();
            }
        }.asLiveData();
    }

    @Override
    public LiveData<Resource<List<Machine>>> getMachinesDescendingType() {
        return new NetworkBoundResource<List<Machine>>() {
            @Override
            protected LiveData<List<Machine>> loadFromDB() {
                return machineDao.getMachinesDescendingType();
            }
        }.asLiveData();
    }

    @Override
    public void remove(List<MachineImage> images) {
        Runnable runnable = () -> machineDao.remove(images);
        appExecutors.diskIO().execute(runnable);
    }

    @Override
    public LiveData<Resource<List<MachineImage>>> getImages(long machineId) {
        return new NetworkBoundResource<List<MachineImage>>() {
            @Override
            protected LiveData<List<MachineImage>> loadFromDB() {
                return machineDao.getImages(machineId);
            }
        }.asLiveData();
    }

    @Override
    public LiveData<Resource<MachineWithImages>> getMachineWithImages(long qrCodeNumber) {
        return new NetworkBoundResource<MachineWithImages>() {
            @Override
            protected LiveData<MachineWithImages> loadFromDB() {
                return machineDao.getMachineWithImages(qrCodeNumber);
            }
        }.asLiveData();
    }

    @Override
    public LiveData<Resource<MachineWithImages>> getMachineWithImages(String qrCodeNumber) {
        return new NetworkBoundResource<MachineWithImages>() {
            @Override
            protected LiveData<MachineWithImages> loadFromDB() {
                return machineDao.getMachineWithImages(qrCodeNumber);
            }
        }.asLiveData();
    }

    @Override
    public LiveData<Resource<List<Gallery>>> getGallery() {
        return new NetworkBoundResource<List<Gallery>>(){
            @Override
            protected LiveData<List<Gallery>> loadFromDB() {
                MutableLiveData<List<Gallery>>imageLiveDate=new MutableLiveData<>();
                List<Gallery>galleries=new ArrayList<>();
                for(String path : ImageThumbails.getGalleryImagePaths(application.getApplicationContext())){
                    galleries.add(new Gallery(path,false));
                }
                imageLiveDate.setValue(galleries);
                return imageLiveDate;
            }
        }.asLiveData();
    }
}
