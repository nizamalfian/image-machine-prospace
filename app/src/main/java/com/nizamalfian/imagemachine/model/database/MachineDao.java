package com.nizamalfian.imagemachine.model.database;

import androidx.lifecycle.LiveData;
import androidx.room.*;
import com.nizamalfian.imagemachine.model.entity.Machine;
import com.nizamalfian.imagemachine.model.entity.MachineImage;
import com.nizamalfian.imagemachine.model.entity.MachineWithImages;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nizamalfian on 19/08/2019.
 */
@Dao
public abstract class MachineDao {

    public void saveMachineAndImages(Machine machine,List<byte[]>images){
        remove(machine.getMachineId());
        long id=save(machine);
        ArrayList<MachineImage>machineImages=new ArrayList<>();
        for(byte[] image : images){
            MachineImage machineImage=new MachineImage(id,image);
            machineImages.add(machineImage);
        }
        saveImages(machineImages);
    }

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract long save(Machine machine);

    @Delete
    public abstract void remove(Machine machine);

    @Query("SELECT * FROM Machine")
    public abstract LiveData<List<Machine>>getMachines();

    @Query("SELECT * FROM Machine ORDER BY name ASC")
    public abstract LiveData<List<Machine>>getMachinesAscendingName();

    @Query("SELECT * FROM Machine ORDER BY name DESC")
    public abstract LiveData<List<Machine>>getMachinesDescendingName();

    @Query("SELECT * FROM Machine ORDER BY type ASC")
    public abstract LiveData<List<Machine>> getMachinesAscendingType();

    @Query("SELECT * FROM Machine ORDER BY type DESC")
    public abstract LiveData<List<Machine>> getMachinesDescendingType();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract long[] saveImages(List<MachineImage> images);

    @Query("DELETE FROM MachineImage WHERE machineId=:machineId")
    public abstract void remove(long machineId);

    @Delete
    public abstract void remove(List<MachineImage> images);

    @Query("SELECT * FROM MachineImage WHERE machineId=:machineId")
    public abstract LiveData<List<MachineImage>>getImages(long machineId);

    @Transaction
    @Query("SELECT * FROM Machine WHERE machineId=:machineId")
    public abstract LiveData<MachineWithImages> getMachineWithImages(long machineId);

    @Transaction
    @Query("SELECT * FROM Machine WHERE qrCodeNumber=:qrCodeNumber")
    public abstract LiveData<MachineWithImages> getMachineWithImages(String qrCodeNumber);
}
