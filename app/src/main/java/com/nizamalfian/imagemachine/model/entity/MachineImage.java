package com.nizamalfian.imagemachine.model.entity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.*;

/**
 * Created by nizamalfian on 19/08/2019.
 */
@Entity(
        foreignKeys = {
                @ForeignKey(
                        entity = Machine.class,
                        parentColumns = "machineId",
                        childColumns = "machineId"
                )
        },
        indices = {
                @Index(value = "imageId"),
                @Index(value = "machineId")
        }
)
public class MachineImage {
    @NonNull
    @PrimaryKey(autoGenerate = true)
    private long imageId;
    @NonNull
    private long machineId;
    @ColumnInfo(typeAffinity = ColumnInfo.BLOB)
    private byte[] image;

    public MachineImage(long machineId, byte[] image) {
        this.imageId = 0;
        this.machineId = machineId;
        this.image = image;
    }

    public void setImageId(@NonNull long imageId) {
        this.imageId = imageId;
    }

    public void setMachineId(int machineId) {
        this.machineId = machineId;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    @NonNull
    public long getImageId() {
        return imageId;
    }

    public long getMachineId() {
        return machineId;
    }

    @Nullable public byte[] getImage() {
        return image;
    }
}
