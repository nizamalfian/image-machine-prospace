package com.nizamalfian.imagemachine.model;

import androidx.lifecycle.LiveData;
import com.nizamalfian.imagemachine.model.entity.Machine;
import com.nizamalfian.imagemachine.model.entity.MachineImage;
import com.nizamalfian.imagemachine.model.entity.MachineWithImages;
import com.nizamalfian.imagemachine.model.pojo.Gallery;
import com.nizamalfian.imagemachine.model.vo.Resource;

import java.util.List;

/**
 * Created by nizamalfian on 19/08/2019.
 */
public interface MachineRepositoryService {
    void saveMachineWithImages(Machine machine,List<byte[]>images);
    void remove(Machine machine);
    LiveData<Resource<List<Machine>>>getMachines();
    LiveData<Resource<List<Machine>>>getMachinesAscendingName();
    LiveData<Resource<List<Machine>>>getMachinesDescendingName();
    LiveData<Resource<List<Machine>>> getMachinesAscendingType();
    LiveData<Resource<List<Machine>>> getMachinesDescendingType();
    void remove(List<MachineImage> images);
    LiveData<Resource<List<MachineImage>>>getImages(long machineId);
    LiveData<Resource<MachineWithImages>>getMachineWithImages(String qrCodeNumber);
    LiveData<Resource<MachineWithImages>>getMachineWithImages(long qrCodeNumber);
    LiveData<Resource<List<Gallery>>>getGallery();
}
