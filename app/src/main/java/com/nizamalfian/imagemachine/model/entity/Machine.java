package com.nizamalfian.imagemachine.model.entity;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Date;

/**
 * Created by nizamalfian on 19/08/2019.
 */
@Entity
public class Machine {
    @PrimaryKey(autoGenerate = true)
    @NonNull
    private long machineId;
    private String name;
    private String type;
    private String qrCodeNumber;
    private Date lastMaintenanceDate;

    public Machine(String name, String type, String qrCodeNumber, Date lastMaintenanceDate) {
        this.machineId = 0;
        this.name = name;
        this.type = type;
        this.qrCodeNumber = qrCodeNumber;
        this.lastMaintenanceDate = lastMaintenanceDate;
    }

    public void setMachineId(@NonNull long machineId) {
        this.machineId = machineId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setQrCodeNumber(String qrCodeNumber) {
        this.qrCodeNumber = qrCodeNumber;
    }

    public void setLastMaintenanceDate(Date lastMaintenanceDate) {
        this.lastMaintenanceDate = lastMaintenanceDate;
    }

    @NonNull
    public long getMachineId() {
        return machineId;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getQrCodeNumber() {
        return qrCodeNumber;
    }

    public Date getLastMaintenanceDate() {
        return lastMaintenanceDate;
    }
}
