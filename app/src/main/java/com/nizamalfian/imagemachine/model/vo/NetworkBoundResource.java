package com.nizamalfian.imagemachine.model.vo;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

public abstract class NetworkBoundResource<ResultType> {
    private MediatorLiveData<Resource<ResultType>> result = new MediatorLiveData<>();

    protected NetworkBoundResource() {
        result.setValue(Resource.loading(null));
        LiveData<ResultType> dbSource = loadFromDB();
        result.addSource(dbSource, data -> {
            result.removeSource(dbSource);
            result.addSource(dbSource, newData -> result.setValue(Resource.success(newData)));
        });
    }

    protected abstract LiveData<ResultType> loadFromDB();

    public LiveData<Resource<ResultType>> asLiveData() {
        return result;
    }
}