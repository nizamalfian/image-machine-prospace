package com.nizamalfian.imagemachine.model.database;

import android.content.Context;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import com.nizamalfian.imagemachine.BuildConfig;
import com.nizamalfian.imagemachine.model.entity.Machine;
import com.nizamalfian.imagemachine.model.entity.MachineImage;
import com.nizamalfian.imagemachine.utils.converter.DateConverters;

/**
 * Created by nizamalfian on 19/08/2019.
 */
@Database(entities = {Machine.class, MachineImage.class},
        version = BuildConfig.ROOM_SCHEMA_VERSION,
        exportSchema = false)
@TypeConverters({DateConverters.class})
public abstract class MachineDatabase extends RoomDatabase {
    private static MachineDatabase INSTANCE;

    public abstract MachineDao machineDao();

    private static final Object lock = new Object();

    public static MachineDatabase getInstance(Context context){
        synchronized (lock){
            if(INSTANCE==null){
                INSTANCE= Room.databaseBuilder(context.getApplicationContext(),
                        MachineDatabase.class,BuildConfig.ROOM_DATABASE_NAME)
                        .build();
            }
            return INSTANCE;
        }
    }

}
