package com.nizamalfian.imagemachine.model.pojo;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by nizamalfian on 26/08/2019.
 */
public class Gallery implements Parcelable {
    private String url;
    private boolean isSelected;

    public Gallery(){}

    public Gallery(String url, boolean isSelected) {
        this.url = url;
        this.isSelected = isSelected;
    }

    private Gallery(Parcel in) {
        url = in.readString();
        isSelected = in.readByte() != 0;
    }

    public static final Creator<Gallery> CREATOR = new Creator<Gallery>() {
        @Override
        public Gallery createFromParcel(Parcel in) {
            return new Gallery(in);
        }

        @Override
        public Gallery[] newArray(int size) {
            return new Gallery[size];
        }
    };

    public boolean isSelected() {
        return isSelected;
    }

    public String getUrl() {
        return url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(url);
        parcel.writeByte((byte) (isSelected ? 1 : 0));
    }
}
