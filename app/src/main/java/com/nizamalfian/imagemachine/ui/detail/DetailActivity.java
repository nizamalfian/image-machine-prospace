package com.nizamalfian.imagemachine.ui.detail;

import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.BindViews;
import butterknife.OnClick;
import com.nizamalfian.imagemachine.R;
import com.nizamalfian.imagemachine.base.BaseActivity;
import com.nizamalfian.imagemachine.model.entity.MachineImage;
import com.nizamalfian.imagemachine.model.entity.MachineWithImages;
import com.nizamalfian.imagemachine.model.vo.Resource;
import com.nizamalfian.imagemachine.ui.form.MachineFormActivity;
import com.nizamalfian.imagemachine.ui.form.MachineImageAdapter;
import com.nizamalfian.imagemachine.ui.form.imageviewholder.ViewHolderWithImage;
import com.nizamalfian.imagemachine.ui.form.imageviewholder.ViewHolderWithNoImage;
import com.nizamalfian.imagemachine.ui.imagepopup.ImageDialog;
import com.nizamalfian.imagemachine.utils.date.DateUtils;
import com.nizamalfian.imagemachine.viewmodel.MachineViewModel;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.nizamalfian.imagemachine.ui.form.FormType.*;

import java.util.ArrayList;
import java.util.List;

public class DetailActivity extends BaseActivity implements ImageDialog.Callback {
    private MachineViewModel viewModel;
    @BindViews({
            R.id.txtName,
            R.id.txtType,
            R.id.txtQRCode,
            R.id.txtLastMaintenanceDate,
            R.id.txtId})
    List<TextView> txts;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.txtEmpty)
    TextView txtEmpty;
    @BindView(R.id.btnEdit)
    Button btnEdit;
    private MachineImageAdapter adapter;
    @Nullable
    private MachineWithImages machineWithImages;

    private final int REQUEST_CODE_EDIT = 437;
    private static final String EXTRA_MACHINE_ID = "EXTRA_MACHINE_ID";
    private static final String EXTRA_QR_CODE_NUMBER = "EXTRA_QR_CODE_NUMBER";
    private ViewHolderWithImage.Callback imageService = (image, position) -> ImageDialog.attach(this, position, false, image);
    private ViewHolderWithNoImage.Callback noImageService = () -> {
    };
    private Menu menu;

    public static Intent getIntent(BaseActivity activity, long machineId, String qrCodeNumber) {
        Intent intent = new Intent(activity, DetailActivity.class);
        intent.putExtra(EXTRA_MACHINE_ID, machineId);
        intent.putExtra(EXTRA_QR_CODE_NUMBER, qrCodeNumber);
        return intent;
    }

    private long getMachineId() {
        return getIntent().getLongExtra(EXTRA_MACHINE_ID, 0);
    }

    private String getQRCodeNumber() {
        return getIntent().getStringExtra(EXTRA_QR_CODE_NUMBER);
    }

    public static void launchFromItem(BaseActivity activity, long machineId, boolean hasToFinish) {
        activity.startActivity(getIntent(activity, machineId, ""));
        if (hasToFinish)
            activity.finish();
    }

    public static void launchFromScanner(BaseActivity activity, String qrCodeNumber, boolean hasToFinish) {
        activity.startActivity(getIntent(activity, 0, qrCodeNumber));
        if (hasToFinish)
            activity.finish();
    }

    @LayoutRes
    @Override
    protected int getLayout() {
        return R.layout.activity_detail;
    }

    @Override
    protected void init() {
        viewModel = createViewModel(MachineViewModel.class);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.machine_detail));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        adapter = new MachineImageAdapter(imageService, noImageService);
        recyclerView.setAdapter(adapter);
    }

    private void observeResult(Resource<MachineWithImages> machineDetail) {
        switch (machineDetail.status) {
            case LOADING:
                loading(true);
                break;
            case SUCCESS:
                if (machineDetail.data != null) {
                    machineWithImages = machineDetail.data;
                    List<byte[]> images = new ArrayList<>();
                    for (MachineImage machineImage : machineWithImages.getImages()) {
                        images.add(machineImage.getImage());
                    }
                    adapter.updateData(images);
                    txtEmpty.setVisibility(machineWithImages.getImages().isEmpty() ? VISIBLE : GONE);
                    txts.get(FORM_ID).setText(String.valueOf(machineWithImages.getMachine().getMachineId()));
                    txts.get(FORM_NAME).setText(machineWithImages.getMachine().getName());
                    txts.get(FORM_TYPE).setText(machineWithImages.getMachine().getType());
                    txts.get(FORM_QR_CODE).setText(String.valueOf(machineWithImages.getMachine().getQrCodeNumber()));
                    txts.get(FORM_LAST_MAINTENANCE_DATE).setText(DateUtils.convertToString(machineWithImages.getMachine().getLastMaintenanceDate()));
                    btnEdit.setVisibility(VISIBLE);
                } else {
                    Toast.makeText(this, String.format(getString(R.string.no_machine), getQRCodeNumber()), Toast.LENGTH_SHORT).show();
                    btnEdit.setVisibility(GONE);
                }
                break;
            case ERROR:
                Toast.makeText(this, getString(R.string.something_error), Toast.LENGTH_SHORT).show();
                loading(false);
                break;
        }
    }

    @Override
    protected void setData() {
        if (getQRCodeNumber().equals("")) {
            viewModel.setMachineId(getMachineId());
            viewModel.machineDetailById.observe(this, this::observeResult);
        } else {
            viewModel.setQrCodeNumber(getQRCodeNumber());
            viewModel.machineDetailByQRCode.observe(this, this::observeResult);
        }
    }

    @Override
    public void loading(boolean show) {
        if (show) {
            for (TextView txt : txts) {
                txt.setText("...");
            }
        }
        btnEdit.setVisibility(show ? GONE : VISIBLE);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            onBackPressed();
        else if (item.getItemId() == R.id.actionRemove) {
            if (machineWithImages != null)
                removeMachineConfirmation(machineWithImages);
            else
                Toast.makeText(this, getString(R.string.machine_is_invalid), Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.menu_detail, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private void removeMachineConfirmation(MachineWithImages machineWithImages) {
        new AlertDialog.Builder(this)
                .setTitle(getString(R.string.remove_machine))
                .setMessage(getString(R.string.are_you_sure_to_remove))
                .setNegativeButton(getString(R.string.no), (dialog, i) -> dialog.dismiss())
                .setPositiveButton(getString(R.string.yes), (dialog, i) -> {
                    viewModel.removeMachine(machineWithImages);
                    finish();
                })
                .create().show();
    }

    @OnClick(R.id.btnEdit)
    void onClickEdit() {
        MachineFormActivity.launchToUpdate(this, Long.parseLong(txts.get(FORM_ID).getText().toString()), REQUEST_CODE_EDIT);
    }

    @Override
    public void removeImage(int position) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CODE_EDIT) {
                finish();
            }
        }
    }
}
