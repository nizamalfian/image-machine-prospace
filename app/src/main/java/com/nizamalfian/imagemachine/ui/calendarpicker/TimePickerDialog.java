package com.nizamalfian.imagemachine.ui.calendarpicker;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.TimePicker;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

import java.lang.ref.WeakReference;
import java.util.Calendar;

/**
 * Created by nizamalfian on 24/08/2019.
 */
public class TimePickerDialog extends DialogFragment implements android.app.TimePickerDialog.OnTimeSetListener {
    @Nullable
    private WeakReference<DialogTimeListener> service;

    private static final String TAG="TIME_PICKER";

    public static void show(FragmentManager fragmentManager){
        new TimePickerDialog().show(fragmentManager,TAG);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        service=new WeakReference<>((DialogTimeListener)getContext());
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Calendar calendar= Calendar.getInstance();
        int hour=calendar.get(Calendar.HOUR_OF_DAY);
        int minute=calendar.get(Calendar.MINUTE);
        return new android.app.TimePickerDialog(getActivity(), this, hour, minute, true);
    }

    @Override
    public void onTimeSet(TimePicker timePicker, int hour, int minute) {
        if (service != null){
            service.get().onDialogTimeSet(hour, minute);
        }
    }

    public interface DialogTimeListener{
        void onDialogTimeSet(int hour, int minute);
    }
}
