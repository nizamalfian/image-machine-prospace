package com.nizamalfian.imagemachine.ui.form;

/**
 * Created by nizamalfian on 25/08/2019.
 */
public class FormType {
    public static final int FORM_NAME = 0;
    public static final int FORM_TYPE = 1;
    public static final int FORM_QR_CODE = 2;
    public static final int FORM_LAST_MAINTENANCE_DATE = 3;
    public static final int FORM_ID = 4;
}
