package com.nizamalfian.imagemachine.ui.main;

import android.Manifest;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.OnClick;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.nizamalfian.imagemachine.R;
import com.nizamalfian.imagemachine.base.BaseActivity;
import com.nizamalfian.imagemachine.model.entity.Machine;
import com.nizamalfian.imagemachine.ui.form.MachineFormActivity;
import com.nizamalfian.imagemachine.ui.detail.DetailActivity;
import com.nizamalfian.imagemachine.ui.qrcodescanner.QRCodeScannerActivity;
import com.nizamalfian.imagemachine.utils.permission.PermissionUtil;
import com.nizamalfian.imagemachine.viewmodel.MachineViewModel;

import java.util.List;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.nizamalfian.imagemachine.model.vo.Status.SUCCESS;
import static com.nizamalfian.imagemachine.utils.view.ViewUtils.setBottomPadding;
import static com.nizamalfian.imagemachine.utils.view.ViewUtils.setDividerItem;
import static com.nizamalfian.imagemachine.viewmodel.MachineViewModel.*;

/**
 * Created by nizamalfian on 19/08/2019.
 */
public class MainActivity extends BaseActivity implements MainView {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.layoutEmpty)
    LinearLayout empty;
    @BindView(R.id.layoutSort)
    LinearLayout sort;
    @BindView(R.id.loading)
    ProgressBar loading;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.txtSortName)
    TextView txtSortName;
    @BindView(R.id.txtSortType)
    TextView txtSortType;
    @BindView(R.id.imgSortName)
    ImageView imgSortName;
    @BindView(R.id.imgSortType)
    ImageView imgSortType;
    @NonNull
    private MachineAdapter adapter;
    private MachineViewModel viewModel;
    public static final int REQUEST_CODE_QR_CODE_NUMBER=279;
    @Nullable private Menu menu;
    private int REQUEST_CODE_ADD=88;

    @LayoutRes
    @Override
    protected int getLayout() {
        return R.layout.activity_main;
    }

    @Override
    protected void init() {
        viewModel = createViewModel(MachineViewModel.class);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        setBottomPadding(recyclerView, 80);
        setDividerItem(recyclerView);
        adapter = new MachineAdapter(machine -> DetailActivity.launchFromItem(this,machine.getMachineId(),false));
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void setData() {
        viewModel.machines.observe(this, machines -> {
            switch (machines.status) {
                case LOADING:
                    loading(true);
                    break;
                case SUCCESS:
                    if (machines.data!=null && !machines.data.isEmpty()) {
                        recyclerView.setVisibility(View.VISIBLE);
                        showData(machines.data);
                    } else {
                        emptyData();
                        if(this.menu!=null && menu.findItem(R.id.actionScan)!=null)
                            menu.removeItem(R.id.actionScan);
                    }
                    loading(false);
                    break;
                case ERROR:
                    Toast.makeText(this, getString(R.string.error), Toast.LENGTH_SHORT).show();
                    loading(false);
                    break;
            }
        });
    }

    @Override
    public void sortInformation(boolean isSortByName, boolean isAscendingSorting) {
        txtSortName.setTextColor(ContextCompat.getColor(this, isSortByName ? R.color.colorAccent : R.color.colorShadow));
        txtSortName.setTypeface(ResourcesCompat.getFont(this, isSortByName ? R.font.nunito_bold : R.font.nunito_extralight));
        imgSortName.setVisibility(isSortByName ? VISIBLE : GONE);
        imgSortName.setImageDrawable(ContextCompat.getDrawable(this, isAscendingSorting ? R.drawable.ic_down_arrow : R.drawable.ic_top_arrow));
        txtSortType.setTextColor(ContextCompat.getColor(this, isSortByName ? R.color.colorShadow : R.color.colorAccent));
        txtSortType.setTypeface(ResourcesCompat.getFont(this, isSortByName ? R.font.nunito_extralight : R.font.nunito_bold));
        imgSortType.setVisibility(isSortByName ? GONE : VISIBLE);
        imgSortType.setImageDrawable(ContextCompat.getDrawable(this, isAscendingSorting ? R.drawable.ic_down_arrow : R.drawable.ic_top_arrow));
    }

    @Override
    public void loading(boolean show) {
        loading.setVisibility(show ? VISIBLE : GONE);
    }

    @Override
    public void emptyData() {
        recyclerView.setVisibility(View.INVISIBLE);
        empty.setVisibility(VISIBLE);
        fab.hide();
        sort.setVisibility(GONE);
//        viewModel.setMachines(FakeMachineData.getMachineDummy());
    }

    @Override
    public void showData(List<Machine> machines) {
        empty.setVisibility(GONE);
        adapter.updateData(machines);
        fab.show();
        sort.setVisibility(VISIBLE);
        recyclerView.post(() -> recyclerView.smoothScrollToPosition(0));

        viewModel.getSortMode().observe(this, sortMode -> {
            switch (sortMode) {
                case SORT_ASC_NAME:
                    sortInformation(true, true);
                    break;
                case SORT_DESC_NAME:
                    sortInformation(true, false);
                    break;
                case SORT_ASC_TYPE:
                    sortInformation(false, true);
                    break;
                case SORT_DESC_TYPE:
                    sortInformation(false, false);
                    break;
            }
        });
    }

    @OnClick(R.id.layoutSortByName)
    void onClickSortByName() {
        if (loading.getVisibility() == GONE)
            sortStateLabel(true);
    }

    @OnClick(R.id.layoutSortByType)
    public void onClickSortByType() {
        if (loading.getVisibility() == GONE)
            sortStateLabel(false);
    }

    private void sortStateLabel(boolean isName) {
        if (isName) {
            viewModel.setNameSortMode();
        } else {
            viewModel.setTypeSortMode();
        }
    }

    @OnClick(R.id.fab)
    void onClickAddNewFab() {
        addNew();
    }

    @OnClick(R.id.btnAdd)
    void onClickAddNewEmpty() {
        addNew();
    }

    private void addNew() {
        if (loading.getVisibility() == GONE)
            MachineFormActivity.launchToAddNew(this,REQUEST_CODE_ADD);
    }

    @Override
    public void readQR() {
        PermissionUtil.permission(this, this::launchQRCode,Manifest.permission.CAMERA);
    }

    private void launchQRCode(){
        QRCodeScannerActivity.launch(this,REQUEST_CODE_QR_CODE_NUMBER);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        viewModel.getMachines().observe(this, listResource -> {
            if(listResource.status==SUCCESS &&
                    listResource.data!=null &&
                    !listResource.data.isEmpty()){
                this.menu=menu;
                if(menu.findItem(R.id.actionScan)!=null)
                    menu.removeItem(R.id.actionScan);
                getMenuInflater().inflate(R.menu.menu_main, menu);
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.actionScan) {
            readQR();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==REQUEST_CODE_QR_CODE_NUMBER && resultCode==RESULT_OK){
            if (data != null && data.getStringExtra(QRCodeScannerActivity.EXTRA_QR_CODE) != null) {
                String qrCodeNumber=data.getStringExtra(QRCodeScannerActivity.EXTRA_QR_CODE);
                DetailActivity.launchFromScanner(this, qrCodeNumber, false);
            }
        }
    }
}
