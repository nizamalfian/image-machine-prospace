package com.nizamalfian.imagemachine.ui.gallery;

import android.content.Intent;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.OnClick;
import com.nizamalfian.imagemachine.R;
import com.nizamalfian.imagemachine.base.BaseActivity;
import com.nizamalfian.imagemachine.model.pojo.Gallery;
import com.nizamalfian.imagemachine.utils.view.ButtonUtil;
import com.nizamalfian.imagemachine.viewmodel.GalleryViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nizamalfian on 26/08/2019.
 */
public class GalleryActivity extends BaseActivity implements GalleryView,GalleryAdapter.Callback{
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.loading)
    ProgressBar loading;
    @BindView(R.id.layoutEmpty)
    LinearLayout layoutEmpty;
    @BindView(R.id.viewShadow)
    View viewShadow;
    @BindView(R.id.frameBtn)
    FrameLayout frameBtn;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.txtMaxItem)
    TextView txtMaxItem;
    private GalleryAdapter adapter;
    private GalleryViewModel viewModel;

    private static final String EXTRA_MAXIMUM_ITEM_SELECTED="EXTRA_MAXIMUM_ITEM_SELECTED";
    public static final String EXTRA_IMAGES="EXTRA_IMAGES";

    public static void launch(BaseActivity activity,int maximumItemSelected,int requestCode){
        Intent intent=new Intent(activity,GalleryActivity.class);
        intent.putExtra(EXTRA_MAXIMUM_ITEM_SELECTED,maximumItemSelected);
        activity.startActivityForResult(intent,requestCode);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_gallery;
    }

    private int getMaximumItem(){
        return getIntent().getIntExtra(EXTRA_MAXIMUM_ITEM_SELECTED,0);
    }

    @Override
    protected void init() {
        viewModel=createViewModel(GalleryViewModel.class);
        adapter=new GalleryAdapter(getMaximumItem());
        onUpdateSelectedItem(0);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        recyclerView.setLayoutManager(new GridLayoutManager(this,3));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void setData() {
        viewModel.getGallery().observe(this, listResource -> {
            switch (listResource.status){
                case LOADING:
                    loading(true);
                    break;
                case SUCCESS:
                    if(listResource.data!=null && !listResource.data.isEmpty())
                        showData(listResource.data);
                    else
                        emptyData();
                    loading(false);
                    break;
                case ERROR:
                    Toast.makeText(this, getString(R.string.something_error), Toast.LENGTH_SHORT).show();
                    loading(false);
                    break;
            }
        });
    }

    @Override
    public void emptyData() {
        layoutEmpty.setVisibility(View.VISIBLE);
    }

    @Override
    public void showData(List<Gallery> images) {
        adapter.updateData(images);
        frameBtn.setVisibility(View.VISIBLE);
        viewShadow.setVisibility(View.VISIBLE);
    }

    @Override
    public void loading(boolean show) {
        if(show){
            layoutEmpty.setVisibility(View.GONE);
            frameBtn.setVisibility(View.GONE);
            viewShadow.setVisibility(View.GONE);
        }
        loading.setVisibility(show?View.VISIBLE:View.GONE);
    }

    @OnClick(R.id.btnSubmit)
    void onClickSubmit(){
        Intent intent=new Intent();
        ArrayList<String>urls=new ArrayList<>();
        for(Gallery selectedImage : adapter.getSelectedItem()){
            urls.add(selectedImage.getUrl());
        }
        intent.putExtra(EXTRA_IMAGES,urls);
        setResult(RESULT_OK,intent);
        finish();
    }

    @Override
    public void onUpdateSelectedItem(int selectedItemCount) {
        String limit=String.format(getString(R.string.selectlimit),selectedItemCount,getMaximumItem());
        txtMaxItem.setText(limit);
        ButtonUtil.buttonEnabling(btnSubmit, () -> selectedItemCount>0);
    }
}
