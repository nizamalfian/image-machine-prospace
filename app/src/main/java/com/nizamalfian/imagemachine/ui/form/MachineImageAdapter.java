package com.nizamalfian.imagemachine.ui.form;

import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.nizamalfian.imagemachine.base.BaseAdapter;
import com.nizamalfian.imagemachine.base.BaseImageViewHolder;
import com.nizamalfian.imagemachine.ui.form.imageviewholder.ViewHolderWithImage;
import com.nizamalfian.imagemachine.ui.form.imageviewholder.ViewHolderWithNoImage;

import java.util.Arrays;
import java.util.List;

import static com.nizamalfian.imagemachine.base.BaseImageViewHolder.VIEW_TYPE_WITH_IMAGE;
import static com.nizamalfian.imagemachine.base.BaseImageViewHolder.VIEW_TYPE_WITH_NO_IMAGE;

/**
 * Created by nizamalfian on 22/08/2019.
 */
public class MachineImageAdapter extends BaseAdapter<byte[]> {
    @Nullable private ViewHolderWithImage.Callback imageService;
    @Nullable private ViewHolderWithNoImage.Callback noImageService;

    public MachineImageAdapter(ViewHolderWithImage.Callback imageService, ViewHolderWithNoImage.Callback noImageService){
        this.imageService = imageService;
        this.noImageService = noImageService;
    }

    int getAvailableImageCount(){
        int count=0;
        for(int i=0;i<super.getList().size();i++){
            if(!Arrays.toString(super.getList().get(i)).equals("[0]"))
                count++;
        }
        return count;
    }

    @Override
    public List<byte[]> getList() {
        List<byte[]>temp=super.getList();
        for(int i=0;i<temp.size();i++){
            if(Arrays.toString(temp.get(i)).equals("[0]")){
                temp.remove(i);
            }
        }
        return temp;
    }

    @NonNull
    @Override
    public BaseImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_WITH_IMAGE:{
                return new ViewHolderWithImage(parent,imageService);
            }
            case VIEW_TYPE_WITH_NO_IMAGE:{
                return new ViewHolderWithNoImage(parent,noImageService);
            }
            default:
                throw new IllegalArgumentException("Invalid view type");
        }
    }

    @Override
    public int getItemViewType(int position) {
        if(getItemCount()==0){
            return VIEW_TYPE_WITH_NO_IMAGE;
        }else{
            if(Arrays.toString(this.list.get(position)).equals("[0]")){
                return VIEW_TYPE_WITH_NO_IMAGE;
            }else{
                return VIEW_TYPE_WITH_IMAGE;
            }
        }
    }

}
