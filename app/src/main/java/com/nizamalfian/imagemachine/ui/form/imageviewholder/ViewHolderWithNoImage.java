package com.nizamalfian.imagemachine.ui.form.imageviewholder;

import android.view.ViewGroup;
import androidx.annotation.Nullable;
import com.nizamalfian.imagemachine.R;
import com.nizamalfian.imagemachine.base.BaseImageViewHolder;

import java.lang.ref.WeakReference;

public class ViewHolderWithNoImage extends BaseImageViewHolder {
    @Nullable
    private WeakReference<ViewHolderWithNoImage.Callback> service;

    public ViewHolderWithNoImage(ViewGroup parent,ViewHolderWithNoImage.Callback callback) {
        super(parent,R.layout.adapter_add_image);
        service=new WeakReference<>(callback);
    }

    @Override
    public void onBind(byte[] data) {
        onClickAction(data,image->{
            if(service!=null)
                service.get().takePicture();
        });
    }

    @Override
    public int getLayoutType() {
        return VIEW_TYPE_WITH_NO_IMAGE;
    }

    public interface Callback{
        void takePicture();
    }
}