package com.nizamalfian.imagemachine.ui.form.imageviewholder;

import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.annotation.Nullable;
import com.nizamalfian.imagemachine.R;
import com.nizamalfian.imagemachine.base.BaseImageViewHolder;
import com.nizamalfian.imagemachine.utils.widget.SquareImageView;

import java.lang.ref.WeakReference;

import static com.nizamalfian.imagemachine.utils.image.ImageUtils.getBitmap;
import static com.nizamalfian.imagemachine.utils.image.ImageUtils.load;

public class ViewHolderWithImage extends BaseImageViewHolder {
    private SquareImageView image;
    private ImageView imgShow;
    @Nullable
    private WeakReference<ViewHolderWithImage.Callback> service;

    public ViewHolderWithImage(ViewGroup parent, ViewHolderWithImage.Callback callback) {
        super(parent, R.layout.adapter_image);
        image = itemView.findViewById(R.id.img);
        imgShow = itemView.findViewById(R.id.imgShow);
        service = new WeakReference<>(callback);
    }

    @Override
    public void onBind(byte[] data) {
        load(image, getBitmap(data),true);
        imgShow.setOnClickListener(v -> {
                    if(service!=null)
                        service.get().show(data,getAdapterPosition());
                });
    }

    @Override
    public int getLayoutType() {
        return VIEW_TYPE_WITH_IMAGE;
    }

    public interface Callback {
        void show(byte[] image, int position);
    }
}