package com.nizamalfian.imagemachine.ui.qrcodescanner;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import com.google.zxing.Result;
import com.nizamalfian.imagemachine.R;
import com.nizamalfian.imagemachine.base.BaseActivity;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

/**
 * Created by nizamalfian on 25/08/2019.
 */
public class QRCodeScannerActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {
    public static final String EXTRA_QR_CODE="EXTRA_QR_CODE";
    private ZXingScannerView scannerView;

    public static void launch(BaseActivity activity,int requestCode){
        activity.startActivityForResult(new Intent(activity,QRCodeScannerActivity.class),requestCode);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        scannerView=new ZXingScannerView(this);
        setContentView(scannerView);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.scan_qr_code));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        scannerView.setResultHandler(this);
        scannerView.startCamera();
    }

    @Override
    protected void onPause() {
        super.onPause();
        scannerView.stopCamera();
    }

    @Override
    public void handleResult(Result result) {
        Intent intent=new Intent();
        intent.putExtra(EXTRA_QR_CODE,result.getText());
        setResult(RESULT_OK,intent);
        Toast.makeText(this, result.getText(), Toast.LENGTH_SHORT).show();
        finish();
    }
}
