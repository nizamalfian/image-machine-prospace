package com.nizamalfian.imagemachine.ui.main;

import com.nizamalfian.imagemachine.base.BaseView;
import com.nizamalfian.imagemachine.model.entity.Machine;

import java.util.List;

/**
 * Created by nizamalfian on 19/08/2019.
 */
public interface MainView extends BaseView {
    void emptyData();
    void showData(List<Machine>machines);
    void readQR();
    void sortInformation(boolean isSortByName, boolean isAscendingSorting);
}
