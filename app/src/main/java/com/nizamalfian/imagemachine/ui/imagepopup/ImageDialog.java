package com.nizamalfian.imagemachine.ui.imagepopup;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.*;
import android.widget.ImageView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import com.nizamalfian.imagemachine.R;
import com.nizamalfian.imagemachine.base.BaseActivity;

import java.lang.ref.WeakReference;

import static android.graphics.Color.TRANSPARENT;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;
import static android.view.Window.FEATURE_NO_TITLE;
import static android.view.WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN;
import static com.nizamalfian.imagemachine.utils.image.ImageUtils.getBitmap;
import static com.nizamalfian.imagemachine.utils.image.ImageUtils.load;

/**
 * Created by nizamalfian on 25/08/2019.
 */
public class ImageDialog extends DialogFragment {
    private Unbinder unbinder;
    @BindView(R.id.img)
    ImageView image;
    @BindView(R.id.imgRemove)
    ImageView imgRemove;
    @Nullable
    private WeakReference<ImageDialog.Callback> service;

    private static String BUNDLE_IMAGE = "BUNDLE_IMAGE";
    private static String BUNDLE_IMAGE_POSITION = "BUNDLE_IMAGE_POSITION";
    private static String BUNDLE_IS_REMOVING_ENABLED = "BUNDLE_IS_REMOVING_ENABLED";

    public static ImageDialog getInstance(int position, byte[] imagesByte, boolean isRemovingEnabled) {
        ImageDialog imageDialog = new ImageDialog();
        Bundle bundle = new Bundle();
        bundle.putInt(BUNDLE_IMAGE_POSITION, position);
        bundle.putBoolean(BUNDLE_IS_REMOVING_ENABLED, isRemovingEnabled);
        bundle.putByteArray(BUNDLE_IMAGE, imagesByte);
        imageDialog.setArguments(bundle);
        return imageDialog;
    }

    public static void attach(BaseActivity activity, int position, boolean isRemovingEnabled, byte[] imagesByte) {
        getInstance(position, imagesByte, isRemovingEnabled).show(activity.getSupportFragmentManager(), "imagedialog");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getContext() instanceof ImageDialog.Callback)
            service = new WeakReference<>((ImageDialog.Callback) getContext());
    }

    @Override
    public void onDetach() {
        if (service != null) {
            service.clear();
            service = null;
        }
        super.onDetach();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_image, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        load(image, getBitmap(getArguments().getByteArray(BUNDLE_IMAGE)), false);
        imgRemove.setVisibility(getArguments().getBoolean(BUNDLE_IS_REMOVING_ENABLED) ? VISIBLE : GONE);
    }

    @OnClick(R.id.imgRemove)
    void onClickRemove() {
        new AlertDialog.Builder(getContext())
                .setTitle(getString(R.string.remove_image))
                .setMessage(getString(R.string.are_you_sure_to_image))
                .setNegativeButton(getString(R.string.no), (dialog, i) -> dialog.dismiss())
                .setPositiveButton(getString(R.string.yes), (dialog, i) -> {
                    if (service != null) {
                        service.get().removeImage(getArguments().getInt(BUNDLE_IMAGE_POSITION));
                        dismiss();
                    }
                })
                .create().show();
    }

    @Override
    public void setupDialog(Dialog dialog, int style) {
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(TRANSPARENT));
            dialog.requestWindowFeature(FEATURE_NO_TITLE);
            dialog.setCanceledOnTouchOutside(true);
            setCancelable(true);
            dialog.getWindow().setSoftInputMode(SOFT_INPUT_STATE_HIDDEN);
            dialog.getWindow().setLayout(WRAP_CONTENT, WRAP_CONTENT);
        }
    }

    @Override
    public void onDestroy() {
        if (unbinder != null) {
            unbinder.unbind();
            unbinder = null;
        }
        super.onDestroy();
    }

    public interface Callback {
        void removeImage(int position);
    }
}
