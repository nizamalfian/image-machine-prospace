package com.nizamalfian.imagemachine.ui.gallery;

import com.nizamalfian.imagemachine.base.BaseView;
import com.nizamalfian.imagemachine.model.pojo.Gallery;

import java.util.List;

/**
 * Created by nizamalfian on 26/08/2019.
 */
public interface GalleryView extends BaseView {
    void emptyData();
    void showData(List<Gallery>images);
}
