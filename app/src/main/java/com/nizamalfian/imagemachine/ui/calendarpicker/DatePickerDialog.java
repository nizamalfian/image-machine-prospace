package com.nizamalfian.imagemachine.ui.calendarpicker;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.DatePicker;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

import java.lang.ref.WeakReference;
import java.util.Calendar;

/**
 * Created by nizamalfian on 24/08/2019.
 */
public class DatePickerDialog extends DialogFragment implements android.app.DatePickerDialog.OnDateSetListener {
    @Nullable
    private WeakReference<DialogDateListener> service;

    private static final String TAG="DATE_PICKER";

    public static void show(FragmentManager fragmentManager){
        new DatePickerDialog().show(fragmentManager,TAG);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        service = new WeakReference<>((DialogDateListener) getContext());
    }

    @Override
    public void onDetach() {
        if (service != null) {
            service.clear();
            service = null;
        }
        super.onDetach();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int date = calendar.get(Calendar.DATE);
        return new android.app.DatePickerDialog(getActivity(), this, year, month,date);
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
        if (service != null)
            service.get().onDialogDateSet(dayOfMonth, month, year);
    }

    public interface DialogDateListener {
        void onDialogDateSet(int date, int month, int year);
    }
}
