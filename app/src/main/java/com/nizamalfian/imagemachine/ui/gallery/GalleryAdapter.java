package com.nizamalfian.imagemachine.ui.gallery;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import com.nizamalfian.imagemachine.R;
import com.nizamalfian.imagemachine.base.BaseAdapter;
import com.nizamalfian.imagemachine.base.BaseViewHolder;
import com.nizamalfian.imagemachine.model.pojo.Gallery;
import com.nizamalfian.imagemachine.utils.image.ImageUtils;
import com.nizamalfian.imagemachine.utils.widget.SquareImageView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * Created by nizamalfian on 26/08/2019.
 */
public class GalleryAdapter extends BaseAdapter<Gallery> {
    private int maxItemSelected;

    public GalleryAdapter(int maxItemSelected) {
        this.maxItemSelected = maxItemSelected;
    }

    public int getSelectedItemCount(){
        return getSelectedItem().size();
    }

    public ArrayList<Gallery>getSelectedItem(){
        ArrayList<Gallery>galleries=new ArrayList<>();
        for(Gallery gallery : getList()){
            if(gallery.isSelected())
                galleries.add(gallery);
        }
        return galleries;
    }

    public void updateData(Gallery gallery,int position){
        this.list.set(position,gallery);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(parent, R.layout.adapter_gallery);
    }

    public class ViewHolder extends BaseViewHolder<Gallery>{
        private WeakReference<GalleryAdapter.Callback> service;
        @BindView(R.id.img) SquareImageView image;
        @BindView(R.id.viewFrame) View viewFrame;
        @BindView(R.id.imgCheck) ImageView imgCheck;

        ViewHolder(ViewGroup parent, int layout) {
            super(parent, layout);
            service=new WeakReference<>((GalleryAdapter.Callback)parent.getContext());
        }

        @Override
        protected void onBind(Gallery data) {
            super.onBind(data);
            ImageUtils.load(image,data.getUrl());
            viewFrame.setVisibility(data.isSelected()?View.VISIBLE:View.GONE);
            imgCheck.setVisibility(data.isSelected()?View.VISIBLE:View.GONE);
            onClickAction(data,gallery->{
                if(getSelectedItemCount()<maxItemSelected || data.isSelected()){
                    Gallery newGallery=new Gallery(data.getUrl(),!data.isSelected());
                    updateData(newGallery,getAdapterPosition());
                    service.get().onUpdateSelectedItem(getSelectedItemCount());
                }else{
                    Toast.makeText(itemView.getContext(), itemView.getContext().getString(R.string.got_maximum_selected_image), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    public interface Callback{
        void onUpdateSelectedItem(int selectedItemCount);
    }
}
