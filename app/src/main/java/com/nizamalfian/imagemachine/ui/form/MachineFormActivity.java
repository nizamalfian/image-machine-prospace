package com.nizamalfian.imagemachine.ui.form;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.BindViews;
import butterknife.OnClick;
import com.nizamalfian.imagemachine.BuildConfig;
import com.nizamalfian.imagemachine.R;
import com.nizamalfian.imagemachine.base.BaseActivity;
import com.nizamalfian.imagemachine.model.entity.Machine;
import com.nizamalfian.imagemachine.model.entity.MachineImage;
import com.nizamalfian.imagemachine.model.entity.MachineWithImages;
import com.nizamalfian.imagemachine.ui.form.imageviewholder.ViewHolderWithImage;
import com.nizamalfian.imagemachine.ui.form.imageviewholder.ViewHolderWithNoImage;
import com.nizamalfian.imagemachine.ui.calendarpicker.DatePickerDialog;
import com.nizamalfian.imagemachine.ui.calendarpicker.TimePickerDialog;
import com.nizamalfian.imagemachine.ui.gallery.GalleryActivity;
import com.nizamalfian.imagemachine.ui.imagepopup.ImageDialog;
import com.nizamalfian.imagemachine.ui.qrcodescanner.QRCodeScannerActivity;
import com.nizamalfian.imagemachine.utils.converter.FileConverter;
import com.nizamalfian.imagemachine.utils.image.FileCompressor;
import com.nizamalfian.imagemachine.utils.permission.PermissionUtil;
import com.nizamalfian.imagemachine.utils.view.ButtonUtil;
import com.nizamalfian.imagemachine.utils.date.DateUtils;
import com.nizamalfian.imagemachine.utils.image.ImageUtils;
import com.nizamalfian.imagemachine.viewmodel.DateViewModel;
import com.nizamalfian.imagemachine.viewmodel.ImageViewModel;
import com.nizamalfian.imagemachine.viewmodel.MachineViewModel;

import java.io.File;
import java.io.IOException;
import java.util.*;

import static com.nizamalfian.imagemachine.ui.form.FormType.*;
import static com.nizamalfian.imagemachine.utils.date.DateUtils.convertToString;

/**
 * Created by nizamalfian on 21/08/2019.
 */
public class MachineFormActivity extends BaseActivity implements MachineFormView, DatePickerDialog.DialogDateListener, ImageDialog.Callback, TimePickerDialog.DialogTimeListener {
    private DateViewModel dateViewModel;
    private final int REQUEST_TAKE_PHOTO = 102;
    public static final String EXTRA_MACHINE_ID = "EXTRA_MACHINE_ID";
    private long machineId;
    private FileCompressor fileCompressor;
    private int REQUEST_CODE_GALLERY = 98;
    private int REQUEST_CODE_SCAN = 66;

    public static Intent getIntent(BaseActivity activity, long machineId) {
        Intent intent = new Intent(activity, MachineFormActivity.class);
        intent.putExtra(EXTRA_MACHINE_ID, machineId);
        return intent;
    }

    public static void launchToUpdate(BaseActivity activity, long machineId, int requestCode) {
        activity.startActivityForResult(getIntent(activity, machineId),requestCode);
    }

    public static void launchToAddNew(BaseActivity activity, int requestCode) {
        activity.startActivityForResult(getIntent(activity, 0),requestCode);
    }

    @BindViews({
            R.id.edtName,
            R.id.edtType,
            R.id.edtQrCodeNumber,
            R.id.edtLastMaintenanceDate})
    List<EditText> edts;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.btnSave)
    Button btnSave;
    private MachineImageAdapter adapter;
    private ImageViewModel imageViewModel;
    private MachineViewModel machineViewModel;

    private ViewHolderWithImage.Callback imageService = (image, position) -> ImageDialog.attach(this, position, true, image);

    private ViewHolderWithNoImage.Callback noImageService = () -> {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            PermissionUtil.permission(this, this::takePictureOptions,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA);
        }else{
            takePictureOptions();
        }
    };

    private long getMachineId(){
        return getIntent().getLongExtra(EXTRA_MACHINE_ID,0);
    }

    private void goToGalleryPage() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            PermissionUtil.permission(this,this::launchGallery,Manifest.permission.READ_EXTERNAL_STORAGE);
        }else{
            launchGallery();
        }
    }

    private void launchGallery(){
        int restAvailableImage = ImageViewModel.MAXIMUM_ITEM - adapter.getAvailableImageCount();
        GalleryActivity.launch(this, restAvailableImage, REQUEST_CODE_GALLERY);
    }

    private void takePictureOptions() {
        String[] options = {getString(R.string.gallery), getString(R.string.camera)};
        new AlertDialog.Builder(this)
                .setTitle(getString(R.string.take_a_picture))
                .setItems(options, (dialog, which) -> {
                    if (which == 0) {
                        goToGalleryPage();
                    } else {
                        dispatchTakePictureIntent();
                    }
                }).show();
    }

    private void dispatchTakePictureIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intent.resolveActivity(getPackageManager()) != null) {
            File file = null;
            try {
                file = FileConverter.createImageFile(this);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (file != null) {
                Uri uri = FileProvider.getUriForFile(MachineFormActivity.this,
                        BuildConfig.APPLICATION_ID + ".provider", file);
                imageViewModel.setFile(file);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                startActivityForResult(intent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_machine_form;
    }

    @Override
    protected void init() {
        dateViewModel = createViewModel(DateViewModel.class);
        imageViewModel = createViewModel(ImageViewModel.class);
        machineViewModel = createViewModel(MachineViewModel.class);

        machineId = getMachineId();

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(isAddNew() ? getString(R.string.add_new_machine) : getString(R.string.edit_machine));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        adapter = new MachineImageAdapter(imageService, noImageService);
        recyclerView.setAdapter(adapter);

        ButtonUtil.buttonEnabling(edts, btnSave, () -> {
            boolean isButtonEnable = true;
            for (EditText edt : edts) {
                if (edt.getText().toString().equals("")) {
                    isButtonEnable = false;
                    break;
                }
            }
            return isButtonEnable;
        });
    }

    private void observeImage() {
        imageViewModel.getImages().observe(this, imagesByte -> {
            adapter.updateData(imagesByte);
        });
    }

    private boolean isAddNew() {
        return machineId==0;
    }

    @Override
    protected void setData() {
        if (isAddNew()) {
            imageViewModel.initImage();
            observeImage();
        } else {
            machineViewModel.setMachineId(machineId);
            machineViewModel.machineDetailById.observe(this, machineWithImagesResource -> {
                switch (machineWithImagesResource.status) {
                    case LOADING:
                        for (EditText edt : edts) {
                            edt.setText("-");
                        }
                        break;
                    case SUCCESS:
                        if (machineWithImagesResource.data != null) {
                            MachineWithImages machineDetail = machineWithImagesResource.data;
                            List<byte[]> images = new ArrayList<>();
                            for (MachineImage machineImage : machineDetail.getImages()) {
                                images.add(machineImage.getImage());
                            }
                            if (images.size() < ImageViewModel.MAXIMUM_ITEM)
                                images.add(new byte[1]);
                            imageViewModel.addImages(images);
                            edts.get(FORM_NAME).setText(machineDetail.getMachine().getName());
                            edts.get(FORM_TYPE).setText(machineDetail.getMachine().getType());
                            edts.get(FORM_QR_CODE).setText(String.valueOf(machineDetail.getMachine().getQrCodeNumber()));
                            edts.get(FORM_LAST_MAINTENANCE_DATE).setText(convertToString(machineDetail.getMachine().getLastMaintenanceDate()));
                            observeImage();
                        }
                        break;
                    case ERROR:
                        Toast.makeText(this, getString(R.string.something_error), Toast.LENGTH_SHORT).show();
                        break;
                }
            });
        }
    }

    @OnClick(R.id.btnSave)
    void onClickSave() {
        if (btnSave.isEnabled()) {
            String name = edts.get(FORM_NAME).getText().toString();
            String type = edts.get(FORM_TYPE).getText().toString();
            String qrCode = edts.get(FORM_QR_CODE).getText().toString();
            String lastMaintenance = edts.get(FORM_LAST_MAINTENANCE_DATE).getText().toString();
            Machine machine = new Machine(name, type, qrCode, DateUtils.convertToDate(lastMaintenance));
            if (machineId!=0)
                machine.setMachineId(machineId);
            save(machine, adapter.getList());
        } else
            Toast.makeText(this, getString(R.string.please_fill_all_data), Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.tilLastMaintenanceDate)
    void onClickTilDate() {
        showDatePicker();
    }

    @OnClick(R.id.edtLastMaintenanceDate)
    void onClickEdtDate() {
        showDatePicker();
    }

    private void showDatePicker() {
        DatePickerDialog.show(getSupportFragmentManager());
    }

    @Override
    public void save(Machine machine, List<byte[]> images) {
        machineViewModel.saveMachineWithImages(machine, images);
        Toast.makeText(this, getString(R.string.success_add), Toast.LENGTH_SHORT).show();
        Intent intent=new Intent();
        setResult(RESULT_OK,intent);
        finish();
    }

    @Override
    public void loading(boolean show) {
        super.loading(show);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_TAKE_PHOTO) {
                if (imageViewModel.getFile() != null) {
                    try {
                        if (fileCompressor == null)
                            fileCompressor = new FileCompressor(this);
                        imageViewModel.setFile(fileCompressor.compressToFile(imageViewModel.getFile()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Bitmap bitmap = BitmapFactory.decodeFile(imageViewModel.getFile().getPath());
                    byte[] imageBitmap = ImageUtils.getByte(bitmap);
                    imageViewModel.addImage(imageBitmap);
                }
            } else if (requestCode == REQUEST_CODE_GALLERY) {
                if (data != null && data.getStringArrayListExtra(GalleryActivity.EXTRA_IMAGES) != null) {
                    ArrayList<String> paths = new ArrayList<>(data.getStringArrayListExtra(GalleryActivity.EXTRA_IMAGES));
                    for (String path : paths) {
                        imageViewModel.addImage(ImageUtils.getImageByte(path));
                    }
                }
            } else if (requestCode == REQUEST_CODE_SCAN) {
                if (data != null && data.getStringExtra(QRCodeScannerActivity.EXTRA_QR_CODE) != null) {
                    edts.get(FORM_QR_CODE).setText(data.getStringExtra(QRCodeScannerActivity.EXTRA_QR_CODE));
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDialogDateSet(int date, int month, int year) {
        dateViewModel.setDate(year, month, date);
        TimePickerDialog.show(getSupportFragmentManager());
    }

    @Override
    public void removeImage(int position) {
        imageViewModel.removeImage(position);
    }

    @Override
    public void onDialogTimeSet(int hour, int minute) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(dateViewModel.getYear(), dateViewModel.getMonth(), dateViewModel.getDate(), hour, minute);
        String timeStamp = DateUtils.convertToString(calendar);
        edts.get(FORM_LAST_MAINTENANCE_DATE).setText(timeStamp);
    }

    @OnClick(R.id.imgScan)
    void onClickScan() {
        PermissionUtil.permission(this, this::launchQRCode,Manifest.permission.CAMERA);
    }

    private void launchQRCode() {
        QRCodeScannerActivity.launch(this, REQUEST_CODE_SCAN);
    }
}
