package com.nizamalfian.imagemachine.ui.main;

import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import butterknife.BindView;
import com.nizamalfian.imagemachine.R;
import com.nizamalfian.imagemachine.base.BaseAdapter;
import com.nizamalfian.imagemachine.base.BaseViewHolder;
import com.nizamalfian.imagemachine.base.BaseViewHolder.BaseViewHolderOnClick;
import com.nizamalfian.imagemachine.model.entity.Machine;
import static com.nizamalfian.imagemachine.utils.date.DateUtils.getDate;
import static com.nizamalfian.imagemachine.utils.date.DateUtils.getHour;

/**
 * Created by nizamalfian on 19/08/2019.
 */
public class MachineAdapter extends BaseAdapter<Machine> {
    private BaseViewHolderOnClick<Machine> callback;

    MachineAdapter(BaseViewHolderOnClick<Machine> callback) {
        this.callback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(parent);
    }

    public class ViewHolder extends BaseViewHolder<Machine> {
        @BindView(R.id.txtName) TextView name;
        @BindView(R.id.txtType) TextView type;
        @BindView(R.id.txtLastMaintenanceDate) TextView lastMaintenanceDate;
        @BindView(R.id.txtLastMaintenanceHour) TextView lastMaintenanceHour;

        ViewHolder(ViewGroup parent) {
            super(parent, R.layout.adapter_machine);
        }

        @Override
        protected void onBind(Machine data) {
            super.onBind(data);
            name.setText(data.getName());
            type.setText(data.getType());
            lastMaintenanceDate.setText(getDate(data.getLastMaintenanceDate()));
            lastMaintenanceHour.setText(getHour(data.getLastMaintenanceDate()));

            onClickAction(data,machine->callback.onClickAction(machine));
        }
    }
}
