package com.nizamalfian.imagemachine.ui.form;

import com.nizamalfian.imagemachine.base.BaseView;
import com.nizamalfian.imagemachine.model.entity.Machine;

import java.util.List;

/**
 * Created by nizamalfian on 24/08/2019.
 */
public interface MachineFormView extends BaseView {
    void save(Machine machine, List<byte[]>images);
}
