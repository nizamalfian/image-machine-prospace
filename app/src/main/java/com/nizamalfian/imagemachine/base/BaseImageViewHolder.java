package com.nizamalfian.imagemachine.base;

import android.view.ViewGroup;
import androidx.annotation.LayoutRes;

public abstract class BaseImageViewHolder extends BaseViewHolder<byte[]> {
    public static final int VIEW_TYPE_WITH_NO_IMAGE = 0;
    public static final int VIEW_TYPE_WITH_IMAGE = 1;

    public BaseImageViewHolder(ViewGroup parent, @LayoutRes int layout) {
        super(parent,layout);
    }

    public abstract int getLayoutType();
}