package com.nizamalfian.imagemachine.base;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.annotation.LayoutRes;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.ButterKnife;

public abstract class BaseViewHolder<T> extends RecyclerView.ViewHolder {

    public BaseViewHolder(ViewGroup parent,@LayoutRes int layout) {
        super(LayoutInflater.from(parent.getContext()).inflate(layout,parent,false));
        ButterKnife.bind(this, itemView);
    }

    protected void onBind(T data) {

    }

    protected void onClickAction(T data, BaseViewHolderOnClick<T> onClickAction) {
        itemView.setOnClickListener(v -> onClickAction.onClickAction(data));
    }

    public interface BaseViewHolderOnClick<T> {
        void onClickAction(T data);
    }
}