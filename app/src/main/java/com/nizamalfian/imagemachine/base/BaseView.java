package com.nizamalfian.imagemachine.base;

/**
 * Created by nizamalfian on 24/08/2019.
 */
public interface BaseView {
    void loading(boolean show);
}
