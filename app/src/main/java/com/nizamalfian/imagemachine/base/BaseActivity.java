package com.nizamalfian.imagemachine.base;

import android.os.Bundle;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProviders;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.nizamalfian.imagemachine.viewmodel.ViewModelFactory;

/**
 * Created by nizamalfian on 24/08/2019.
 */
public abstract class BaseActivity extends AppCompatActivity implements BaseView {
    private Unbinder unBinder;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayout());
        setUnBinder(ButterKnife.bind(this));
        init();
        setData();
    }

    protected void setUnBinder(Unbinder unBinder) {
        this.unBinder = unBinder;
    }

    protected  <T extends ViewModel> T createViewModel(@NonNull Class<T> modelClass){
        return ViewModelProviders.of(this, ViewModelFactory.getInstance(getApplication())).get(modelClass);
    }

    @Override
    protected void onDestroy() {
        if(unBinder!=null)
            unBinder.unbind();
        super.onDestroy();
    }

    @Override
    public void loading(boolean show) {

    }

    @LayoutRes protected abstract int getLayout();
    protected abstract void init();
    protected abstract void setData();
}
