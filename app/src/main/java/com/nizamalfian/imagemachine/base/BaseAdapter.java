package com.nizamalfian.imagemachine.base;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nizamalfian on 24/08/2019.
 */
public abstract class BaseAdapter<T> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    protected ArrayList<T>list=new ArrayList<>();

    public void updateData(List<T>list){
        this.list.clear();
        this.list.addAll(list);
        notifyDataSetChanged();
    }

    public List<T>getList(){
        return list;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((BaseViewHolder<T>)holder).onBind(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
