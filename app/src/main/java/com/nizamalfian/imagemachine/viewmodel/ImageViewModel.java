package com.nizamalfian.imagemachine.viewmodel;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.nizamalfian.imagemachine.model.MachineRepositoryService;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by nizamalfian on 24/08/2019.
 */
public class ImageViewModel extends BaseViewModel {
    private MutableLiveData<List<byte[]>> imagesObserve = new MutableLiveData<>();
    private ArrayList<byte[]> images = new ArrayList<>();
    public static final int MAXIMUM_ITEM = 3;
    private File file;

    public ImageViewModel(MachineRepositoryService repository) {
        super(repository);
    }

    @Nullable
    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public void initImage() {
        if (images.isEmpty()) {
            images.add(new byte[1]);
            this.imagesObserve.setValue(images);
        }
    }

    public void addImages(List<byte[]> images) {
        this.images.clear();
        this.images.addAll(images);
        imagesObserve.setValue(this.images);
    }

    public void addImage(byte[] image) {
        if (images.size() <= MAXIMUM_ITEM && images.size() > 0) {
            images.remove(images.size() - 1);
        }
        images.add(image);
        if (images.size() < MAXIMUM_ITEM) {
            byte[] last = new byte[1];
            images.add(last);
        }
        this.imagesObserve.setValue(images);
    }

    public void removeImage(int position) {
        images.remove(position);
        if (images.size() < MAXIMUM_ITEM &&
                !images.isEmpty() &&
                !Arrays.toString(images.get(images.size() - 1)).equals("[0]")) {
            byte[] last = new byte[1];
            images.add(last);
        }
        this.imagesObserve.setValue(images);
    }

    public LiveData<List<byte[]>> getImages() {
        return imagesObserve;
    }
}
