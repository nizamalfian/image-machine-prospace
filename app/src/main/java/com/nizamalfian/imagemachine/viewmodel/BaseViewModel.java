package com.nizamalfian.imagemachine.viewmodel;

import androidx.lifecycle.ViewModel;
import com.nizamalfian.imagemachine.model.MachineRepositoryService;

/**
 * Created by nizamalfian on 24/08/2019.
 */
abstract class BaseViewModel extends ViewModel {
    MachineRepositoryService repository;

    BaseViewModel(MachineRepositoryService repository){
        this.repository=repository;
    }
}
