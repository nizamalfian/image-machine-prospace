package com.nizamalfian.imagemachine.viewmodel;

import com.nizamalfian.imagemachine.model.MachineRepositoryService;

import java.util.Calendar;

/**
 * Created by nizamalfian on 25/08/2019.
 */
public class DateViewModel extends BaseViewModel {
    private int year;
    private int month;
    private int date;

    DateViewModel(MachineRepositoryService repository) {
        super(repository);
        Calendar calendar=Calendar.getInstance();
        setDate(calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DATE));
    }

    public void setDate(int year,int month,int date){
        this.year=year;
        this.month=month;
        this.date=date;
    }

    public int getYear() {
        return year;
    }

    public int getMonth() {
        return month;
    }

    public int getDate() {
        return date;
    }
}
