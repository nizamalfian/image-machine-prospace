package com.nizamalfian.imagemachine.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import com.nizamalfian.imagemachine.model.MachineRepositoryService;
import com.nizamalfian.imagemachine.model.entity.Machine;
import com.nizamalfian.imagemachine.model.entity.MachineImage;
import com.nizamalfian.imagemachine.model.entity.MachineWithImages;
import com.nizamalfian.imagemachine.model.vo.Resource;

import java.util.List;

/**
 * Created by nizamalfian on 19/08/2019.
 */
public class MachineViewModel extends BaseViewModel {
    private MutableLiveData<Long>machineId=new MutableLiveData<>();
    private MutableLiveData<String>qrCodeNumber=new MutableLiveData<>();
    private MutableLiveData<Integer>sort=new MutableLiveData<>();

    public static final int SORT_ASC_NAME=0;
    public static final int SORT_DESC_NAME=1;
    public static final int SORT_ASC_TYPE =2;
    public static final int SORT_DESC_TYPE =3;

    MachineViewModel(MachineRepositoryService repository) {
        super(repository);
        setNameSortMode();
    }

    public void setQrCodeNumber(String qrCodeNumber){
        this.qrCodeNumber.setValue(qrCodeNumber);
    }

    public void setNameSortMode(){
        if(getSortMode().getValue()==null){
            this.sort.setValue(SORT_ASC_NAME);
        }else{
            if(getSortMode().getValue()==SORT_ASC_NAME){
                this.sort.setValue(SORT_DESC_NAME);
            }else{
                this.sort.setValue(SORT_ASC_NAME);
            }
        }
    }

    public void setTypeSortMode(){
        if(getSortMode().getValue()==null){
            this.sort.setValue(SORT_ASC_TYPE);
        }else{
            if(getSortMode().getValue()== SORT_ASC_TYPE){
                this.sort.setValue(SORT_DESC_TYPE);
            }else{
                this.sort.setValue(SORT_ASC_TYPE);
            }
        }
    }

    public LiveData<Integer>getSortMode(){
        return sort;
    }

    public void setMachineId(long machineId) {
        this.machineId.setValue(machineId);
    }

    public void saveMachineWithImages(Machine machine, List<byte[]>images){
        repository.saveMachineWithImages(machine,images);
    }

    public void removeMachine(MachineWithImages machineWithImages){
        repository.remove(machineWithImages.getImages());
        repository.remove(machineWithImages.getMachine());
    }

    public LiveData<Resource<List<Machine>>>getMachines(){
        return repository.getMachines();
    }

    public LiveData<Resource<List<Machine>>>machines=
            Transformations.switchMap(sort,sortMode->{
                if(sortMode==SORT_DESC_NAME)
                    return repository.getMachinesDescendingName();
                else if(sortMode== SORT_ASC_TYPE)
                    return repository.getMachinesAscendingType();
                else if(sortMode== SORT_DESC_TYPE)
                    return repository.getMachinesDescendingType();
                else
                    return repository.getMachinesAscendingName();
            });

    public LiveData<Resource<List<MachineImage>>> images=
        Transformations.switchMap(machineId,machineId->repository.getImages(machineId));

    public LiveData<Resource<MachineWithImages>> machineDetailById =
            Transformations.switchMap(machineId,machineId->repository.getMachineWithImages(machineId));

    public LiveData<Resource<MachineWithImages>> machineDetailByQRCode =
            Transformations.switchMap(qrCodeNumber,qrCodeId->repository.getMachineWithImages(qrCodeId));
}
