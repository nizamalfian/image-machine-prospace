package com.nizamalfian.imagemachine.viewmodel;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.nizamalfian.imagemachine.model.MachineRepositoryService;
import com.nizamalfian.imagemachine.di.Injection;

/**
 * Created by nizamalfian on 19/08/2019.
 */
public class ViewModelFactory extends ViewModelProvider.NewInstanceFactory {
    private static volatile ViewModelFactory INSTANCE;
    private final MachineRepositoryService machineRepository;

    private ViewModelFactory(MachineRepositoryService machineRepository) {
        this.machineRepository = machineRepository;
    }

    public static ViewModelFactory getInstance(Application application){
        if(INSTANCE==null){
            synchronized (ViewModelFactory.class){
                if(INSTANCE==null){
                    INSTANCE = new ViewModelFactory(Injection.provideRepository(application));
                }
            }
        }
        return INSTANCE;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if(modelClass.isAssignableFrom(MachineViewModel.class))
            return (T) new MachineViewModel(machineRepository);
        if(modelClass.isAssignableFrom(ImageViewModel.class))
            return (T) new ImageViewModel(machineRepository);
        if(modelClass.isAssignableFrom(DateViewModel.class))
            return (T) new DateViewModel(machineRepository);
        if(modelClass.isAssignableFrom(GalleryViewModel.class))
            return (T) new GalleryViewModel(machineRepository);
        throw new IllegalArgumentException("Unknown ViewModel class: " + modelClass.getName());
    }
}
