package com.nizamalfian.imagemachine.viewmodel;

import androidx.lifecycle.LiveData;
import com.nizamalfian.imagemachine.model.MachineRepositoryService;
import com.nizamalfian.imagemachine.model.pojo.Gallery;
import com.nizamalfian.imagemachine.model.vo.Resource;

import java.util.List;

/**
 * Created by nizamalfian on 26/08/2019.
 */
public class GalleryViewModel extends BaseViewModel {

    public GalleryViewModel(MachineRepositoryService repository) {
        super(repository);
    }

    public LiveData<Resource<List<Gallery>>>getGallery(){
        return repository.getGallery();
    }
}
